BourgTech is your dedicated IT resource, based in Auburn-Opelika, but servicing the wider Columbus, Montgomery and La Grange regions. We repair, install and upgrade computers, networks, wireless networks, printers, phone systems, retail POS systems, smartphones, tablets and more.

Address: PO Box 845, Auburn, AL 36831, USA

Phone: 334-332-4972

Website: http://www.bourgtechllc.com/
